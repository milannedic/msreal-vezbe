#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>	//register_chrdev_region, fops ... char support
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
MODULE_LICENSE("Dual BSD/GPL");

/*********************************************************
Flow:
1. make
2. ./init.sh
3. On next runs do "rmmod lifo", then make and ./init.sh

Commands:

echo "aa,a2,ff,55" > /dev/lifo
    pushes 4 values to the stack

cat /dev/lifo
    pops one element from stack

echo hex > /dev/lifo
    changes print mode to hex

echo dec > /dev/lifo
    changes print mode to decimal

echo num=n > /dev/lifo
    pops n elements from stack
**********************************************************/

/* #define NULL 0 */
#define STACK_DEPTH 8

//fops functions
int storage_open(struct inode *pinode,
                 struct file *pfile);
int storage_close(struct inode *pinode,
                  struct file *pfile);
ssize_t storage_read(struct file *pfile,
                     char __user *buffer,
                     size_t length,
                     loff_t *offset);
static ssize_t storage_write(struct file *pfile,
                             const char __user *buffer,
                             size_t length,
                             loff_t *offset);
//utility functions
static unsigned long strToInt(const char* pStr,
                              int len,
                              int base);
static char chToUpper(char ch);
static int intToStr(int val,
                    char* pBuf,
                    int bufLen,
                    int base);

char* strtok1(char *str, const char* delim);
static void print_stack(void);
void pop(int p_fmt);

// global variables
int endRead = 0;
dev_t dev_id;
struct cdev *my_cdev;

char lifo_buff[STACK_DEPTH][3];
int stack_pointer = 0;

// print format flag and its type
typedef enum {INT = 10, HEX = 16} print_fmt_t;
print_fmt_t p_fmt = INT;

char prijem[100];
int pos = 0;

//fops
struct file_operations my_fops =
{
	.owner = THIS_MODULE,
	.open = storage_open,
	.read = storage_read,
	.write = storage_write,
	.release = storage_close,
};

//************** FILE OPERATIONS *********************

int storage_open(struct inode *pinode, struct file *pfile)
{
	printk(KERN_INFO "Succesfully opened file\n");
	return 0;
}

int storage_close(struct inode *pinode, struct file *pfile)
{
	printk(KERN_INFO "Succesfully closed file\n");
	return 0;
}

ssize_t storage_read(struct file *pfile,
                     char __user *buffer,
                     size_t length,
                     loff_t *offset)
{

	int ret;
	char temp_array[100];
	int len;

  endRead = 1;

  if (endRead) {
		endRead = 0;
		pos = 0;

    len = strlen(lifo_buff[stack_pointer]);
    pop(p_fmt);
    print_stack();

		printk(KERN_INFO "Succesfully read from file\n");
		return 0;
	}

	/* len = intToStr(niz[pos], temp_array, 100, 10); */
	/* temp_array[len] = ','; */
	/* len++; */
	/* ret=copy_to_user(buffer, temp_array, len); */
	/* pos ++; */
	/* if (pos == 10) { */
	/* 	endRead = 1; */
	/* } */
	return len;
}

static ssize_t storage_write(struct file *pfile,
                             const  char __user *buffer,
                             size_t length,
                             loff_t *offset)
{

	char *tmp;
	char buff [length];

	int buff_len;
	int number, place;
	int ret;

  /* Appendix 2 */
  char *first_equal;
  char *pop_cnt;
  /* how many numbers are split-exported */
  int num_exportd = 0;

  ret = copy_from_user(buff, buffer, length);
	buff[length-1] = '\0';

  /* pointer to the first occurence of num= substring */
  first_equal = strstr(buff, "num=");
  if (first_equal != NULL) {
    /* lets move to the right for the length of num= */
    pop_cnt = (first_equal+strlen("num="));

    printk("\nPopping out: %c elements...\n", *pop_cnt);

    int i = 0;
    /* pop n elements */
    /* printk("pop_cnt+0 = %d", (*pop_cnt)+0); */
    number = strToInt(pop_cnt, strlen((pop_cnt)), 10);
    for(i = 0; i < number; i++) pop(p_fmt);

    print_stack();
  } else {

    /* change print mode */
    if(strcmp(buff, "hex") == 0) {
      p_fmt = HEX;
      printk("\nPrint mode set to: hex\n");
      return length;
    } else if(strcmp(buff, "dec") == 0) {
      p_fmt = INT;
      printk("\nPrint mode set to: int\n");
      return length;
    }

    /* tokenize input string, delimiter "," */
    char *pch;
    pch = strtok1(buff, ",");

    /* STACK_DEPTH * 2 just to make it bigger than stack depth */
    while (pch != NULL && num_exportd != STACK_DEPTH*2) {
      if (stack_pointer + 1 > STACK_DEPTH) {
        printk("\nStack is full! STACK_DEPTH = %d\n", STACK_DEPTH);
        break;
      }
      strcpy(lifo_buff[stack_pointer++], pch);
      num_exportd++;

      pch = strtok1(NULL, " ,");
    }

    /* sada je ovo zapravo nebitno... */
    if (num_exportd >= STACK_DEPTH) {
      printk(KERN_ERR "Stack depth is %d elements, you provided: %d\n\n",
            STACK_DEPTH, num_exportd);
    }

    /* print out stack contents after each write */
    print_stack();
  }

 	printk(KERN_INFO "Succesfully wrote into file\n");
	return length;
}

/* Prints current stack' state
 */
static void print_stack(void) {
  int i = 0;
  printk("Stack content:");
  for(i = 0; i < STACK_DEPTH; i++) {
    if (lifo_buff[i] != NULL) {
      if (i == stack_pointer-1) {
        printk("[ 0x%s ] <--", lifo_buff[i]);
      } else if (i > stack_pointer-1) {
        printk("[      ]", lifo_buff[i]);
      } else {
        printk("[ 0x%s ]", lifo_buff[i]);
      }
    } else {
      printk("[     ]\n");
    }

  }
}

/* Stack pop function
 */
void pop(int p_fmt) {
  int number = 0;
  int buff_elem_len = sizeof(lifo_buff[0])/sizeof(lifo_buff[0][0]);
  char tmp[buff_elem_len];

  if(stack_pointer > 0) {
    if (p_fmt == INT) {
      /* moze i ovako */
      /* memcpy(tmp, lifo_buff[--stack_pointer], buff_elem_len); */

      strcpy(tmp, lifo_buff[--stack_pointer]);
      /* *(tmp+strlen(tmp)) = ''; */

      number = strToInt(tmp, buff_elem_len, 16);
      printk("\n\nPopped-off of the stack: %d\n\n", number);
    } else {
      printk("\n\nPopped-off of the stack: 0x%s\n\n", lifo_buff[--stack_pointer]);
    }
    /* erase the popped value from lifo buffer. */
    strcpy(lifo_buff[stack_pointer], "  ");
  } else {
    printk("\nStack is empty!\n");
  }
}


char* sp = NULL; /* the start position of the string */

/* Tokenize given string
 */
char* strtok1(char* str, const char* delimiters) {

    int i = 0;
    int len = strlen(delimiters);

    /* check in the delimiters */
    if(len == 0)
        printk("delimiters are empty\n");

    /* if the original string has nothing left */
    if(!str && !sp)
        return NULL;

    /* initialize the sp during the first call */
    if(str && !sp)
        sp = str;

    /* find the start of the substring, skip delimiters */
    char* p_start = sp;
    while(1) {
        for(i = 0; i < len; i ++) {
            if(*p_start == delimiters[i]) {
                p_start ++;
                break;
            }
        }

        if(i == len) {
               sp = p_start;
               break;
        }
    }

    /* return NULL if nothing left */
    if(*sp == '\0') {
        sp = NULL;
        return sp;
    }

    /* find the end of the substring, and
        replace the delimiter with null */
    while(*sp != '\0') {
        for(i = 0; i < len; i ++) {
            if(*sp == delimiters[i]) {
                *sp = '\0';
                break;
            }
        }

        sp ++;
        if (i < len)
            break;
    }

    return p_start;
}


//************ UTILITY FUNCTIONS ***********************

static char chToUpper(char ch)
{
	if((ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9'))
	{
		return ch;
	}
	else
	{
		return ch - ('a'-'A');
	}
}

static unsigned long strToInt(const char* pStr, int len, int base)
{
	//                      0,1,2,3,4,5,6,7,8,9,:,;,<,=,>,?,@,A ,B ,C ,D ,E ,F
	static const int v[] = {0,1,2,3,4,5,6,7,8,9,0,0,0,0,0,0,0,10,11,12,13,14,15};
	int i   = 0;
	unsigned long val = 0;
	int dec = 1;
	int idx = 0;

	for(i = len; i > 0; i--)
	{
    /* sta ovo uopste radi!? */
    idx = chToUpper(pStr[i-1]) - '0';

		if(idx > sizeof(v)/sizeof(int))
		{
			printk("strToInt: illegal character %c\n", pStr[i-1]);
			continue;
		}

		val += (v[idx]) * dec;
		dec *= base;
	}

	return val;
}

static int intToStr(int val, char* pBuf, int bufLen, int base)
{
	static const char* pConv = "0123456789ABCDEF";
	int num = val;
	int len = 0;
	int pos = 0;

	while(num > 0)
	{
		len++;
		num /= base;
	}

	if(val == 0)
	{
		len = 1;
	}

	pos = len-1;
	num = val;

	if(pos > bufLen-1)
	{
		pos = bufLen-1;
	}

	for(; pos >= 0; pos--)
	{
		pBuf[pos] = pConv[num % base];
		num /= base;
	}

	return len;
}

//************ INIT AND EXIT ***********************
static int __init storage_init(void)
{
	int ret = 0;
	int i;
	ret = alloc_chrdev_region(&dev_id,0,1,"lifo");
	if(ret)
	{
		printk(KERN_ERR "failed to register char device\n");
		return ret;
	}

	my_cdev = cdev_alloc();
	my_cdev -> owner = THIS_MODULE;
	my_cdev -> ops = &my_fops;
	ret = cdev_add(my_cdev,dev_id,1);
	if(ret)
	{
		unregister_chrdev_region(dev_id,1);
		printk(KERN_ERR "failed to add char device\n");
		return ret;
	}

	printk(KERN_INFO "Hello, world\n");
	for (i=0; i<STACK_DEPTH; i++) {
		strcpy(lifo_buff[i], "  ");
  }

	return 0;
}

static void __exit storage_exit(void)
{
	unregister_chrdev_region(dev_id,1);
	cdev_del(my_cdev);

	printk(KERN_INFO "Goodbye, cruel world\n");
}

module_init(storage_init);
module_exit(storage_exit);
