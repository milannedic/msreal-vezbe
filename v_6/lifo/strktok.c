#include <stdio.h>


typedef void(*split_fn)(const char *, size_t, void *);

void split(const char *str, char sep, split_fn fun, void *data)
{
  unsigned int start = 0, stop;
  for (stop = 0; str[stop]; stop++) {
    if (str[stop] == sep) {
      fun(str + start, stop - start, data);
      start = stop + 1;
    }
  }
  fun(str + start, stop - start, data);
}

void print(const char *str, int len, void *data)
{
  printf("%.*s\n", (int)len, str);
}

int main(void)
{
  char str[] = "first,second,third,fourth";
  split(str, ',', print, NULL);
  return 0;
}
